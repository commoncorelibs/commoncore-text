﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Diagnostics.CodeAnalysis;
using CommonCore.Text;
using FluentAssertions.Numeric;
using FluentAssertions.Primitives;

namespace FluentAssertions
{
    [ExcludeFromCodeCoverage]
    public static class FluentAssertionsExtensions
    {
        public static AndConstraint<NumericAssertions<int>> BeZero(this NumericAssertions<int> self, string because = "", params object[] becauseArgs)
            => self.Be(0, because, becauseArgs);

        public static AndConstraint<NumericAssertions<int>> NotBeZero(this NumericAssertions<int> self, string because = "", params object[] becauseArgs)
            => self.NotBe(0, because, becauseArgs);

        public static ObjectAssertions Should(this SubString self)
            => self.Should();
    }
}
