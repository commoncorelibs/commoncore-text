#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Text.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_StringSpan
    {
        private static string Text { get; } = "abcd";

        [Fact]
        public void General()
        {
            SubString.Empty.Source.Should().NotBeNull().And.BeEmpty();
            SubString.Empty.Index.Should().BeZero();
            SubString.Empty.Length.Should().BeZero();
            SubString.Empty.IsEmpty.Should().BeTrue();
        }

        [Fact]
        public void Constructor_String()
        {
            SubString subject;

            subject = new(null);
            subject.Source.Should().NotBeNull().And.BeEmpty();
            subject.Index.Should().BeZero();
            subject.Length.Should().BeZero();
            subject.IsEmpty.Should().BeTrue();
            subject.ToString().Should().BeEmpty();

            subject = new(string.Empty);
            subject.Source.Should().NotBeNull().And.BeEmpty();
            subject.Index.Should().BeZero();
            subject.Length.Should().BeZero();
            subject.IsEmpty.Should().BeTrue();
            subject.ToString().Should().BeEmpty();

            subject = new(Text);
            subject.Source.Should().NotBeNull().And.Be(Text);
            subject.Index.Should().BeZero();
            subject.Length.Should().Be(Text.Length);
            subject.IsEmpty.Should().BeFalse();
            subject.ToString().Should().NotBeEmpty();
        }

        [Theory]
        [InlineData("something", 0, 9)]
        [InlineData("something", 1, 8)]
        [InlineData("something", 8, 1)]
        [InlineData("something", 9, 0)]
        public void Constructor_String_Index(string source, int index, int length)
        {
            SubString subject = new(source, index);
            subject.Source.Should().NotBeNull().And.Be(source);
            subject.Index.Should().Be(index);
            subject.Length.Should().Be(length);
            subject.IsEmpty.Should().Be(length == 0);

            string expected = source.Substring(index, length);
            subject.AsSpan().ToString().Should().Be(expected);
            subject.AsMemory().ToString().Should().Be(expected);
            subject.ToString().Should().Be(expected);
            SubStringFormatter.Default.ToString(subject).Should().Be(expected);

            ((IEnumerable<char>)subject).Should().BeEquivalentTo(expected);
        }

        [Theory]
        [InlineData("something", 0, 9)]
        [InlineData("something", 1, 8)]
        [InlineData("something", 8, 1)]
        [InlineData("something", 9, 0)]
        public void Constructor_String_Index_Length(string source, int index, int length)
        {
            SubString subject = new(source, index, length);
            subject.Source.Should().NotBeNull().And.Be(source);
            subject.Index.Should().Be(index);
            subject.Length.Should().Be(length);
            subject.IsEmpty.Should().Be(length == 0);

            string expected = source.Substring(index, length);
            subject.AsSpan().ToString().Should().Be(expected);
            subject.AsMemory().ToString().Should().Be(expected);
            subject.ToString().Should().Be(expected);
            SubStringFormatter.Default.ToString(subject).Should().Be(expected);

            ((IEnumerable<char>) subject).Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Slice()
        {
            SubString source = new(Text);
            SubString subject;
            string expected;

            expected = Text.Substring(1);
            subject = source.Slice(1);
            subject.Length.Should().Be(expected.Length);
            subject.ToString().Should().Be(expected);

            expected = Text.Substring(1, 2);
            subject = source.Slice(1, 2);
            subject.Length.Should().Be(expected.Length);
            subject.ToString().Should().Be(expected);

        }

        [Fact]
        public void Indexer()
        {
            SubString subject = new(Text, 1, 1);

            subject[0].Should().Be(Text[1]);

            Action act;

            act = delegate { char _ = subject[-1]; };
            act.Should().ThrowExactly<ArgumentIndexOutOfRangeException>();

            act = delegate { char _ = subject[subject.Length]; };
            act.Should().ThrowExactly<ArgumentIndexOutOfRangeException>();
        }

        [Theory]
        [InlineData(true, "example", 0, 1, "example", 6, 1)]
        [InlineData(false, "example", 0, 1, "example", 5, 1)]
        [InlineData(false, "example", 0, 1, "example", 5, 2)]
        public void Equality(bool expected, string sourceA, int indexA, int lengthA, string sourceB, int indexB, int lengthB)
        {
            SubString subjectA = new(sourceA, indexA, lengthA);
            subjectA.Equals(new object()).Should().BeFalse();
            subjectA.Equals(subjectA).Should().BeTrue();
            subjectA.Equals((object) subjectA).Should().BeTrue();
            SubStringEqualityComparer.Default.Equals(subjectA, subjectA).Should().BeTrue();
#pragma warning disable CS1718 // Comparison made to same variable
            (subjectA == subjectA).Should().BeTrue();
            (subjectA != subjectA).Should().BeFalse();
#pragma warning restore CS1718 // Comparison made to same variable

            SubString subjectB = new(sourceB, indexB, lengthB);
            subjectB.Equals(new object()).Should().BeFalse();
            subjectB.Equals(subjectB).Should().BeTrue();
            subjectB.Equals((object) subjectB).Should().BeTrue();
            SubStringEqualityComparer.Default.Equals(subjectB, subjectB).Should().BeTrue();
#pragma warning disable CS1718 // Comparison made to same variable
            (subjectA == subjectA).Should().BeTrue();
            (subjectA != subjectA).Should().BeFalse();
#pragma warning restore CS1718 // Comparison made to same variable

            subjectA.Equals(subjectB).Should().Be(expected);
            SubStringEqualityComparer.Default.Equals(subjectA, subjectB).Should().Be(expected);
            (subjectA == subjectB).Should().Be(expected);
            (subjectA != subjectB).Should().Be(!expected);

            subjectB.Equals(subjectA).Should().Be(expected);
            SubStringEqualityComparer.Default.Equals(subjectB, subjectA).Should().Be(expected);
            (subjectB == subjectA).Should().Be(expected);
            (subjectB != subjectA).Should().Be(!expected);
        }

        [Theory]
        [InlineData(0, "example", 0, 1, "example", 6, 1)]
        [InlineData(-1, "example", 0, 1, "example", 5, 1)]
        [InlineData(-1, "example", 0, 1, "example", 5, 2)]
        public void Compare(int expected, string sourceA, int indexA, int lengthA, string sourceB, int indexB, int lengthB)
        {
            SubString subjectA = new(sourceA, indexA, lengthA);
            subjectA.CompareTo(new object()).Should().BeZero();
            subjectA.CompareTo(subjectA).Should().BeZero();
            subjectA.CompareTo((object) subjectA).Should().BeZero();
            SubStringComparer.Default.Compare(subjectA, subjectA).Should().BeZero();
#pragma warning disable CS1718 // Comparison made to same variable
            (subjectA > subjectA).Should().BeFalse();
            (subjectA >= subjectA).Should().BeTrue();
            (subjectA < subjectA).Should().BeFalse();
            (subjectA <= subjectA).Should().BeTrue();
#pragma warning restore CS1718 // Comparison made to same variable

            SubString subjectB = new(sourceB, indexB, lengthB);
            subjectB.CompareTo(new object()).Should().BeZero();
            subjectB.CompareTo(subjectB).Should().BeZero();
            subjectB.CompareTo((object) subjectB).Should().BeZero();
            SubStringComparer.Default.Compare(subjectB, subjectB).Should().BeZero();
#pragma warning disable CS1718 // Comparison made to same variable
            (subjectB > subjectB).Should().BeFalse();
            (subjectB >= subjectB).Should().BeTrue();
            (subjectB < subjectB).Should().BeFalse();
            (subjectB <= subjectB).Should().BeTrue();
#pragma warning restore CS1718 // Comparison made to same variable

            subjectA.CompareTo(subjectB).Should().Be(expected);
            subjectA.CompareTo((object) subjectB).Should().Be(expected);
            SubStringComparer.Default.Compare(subjectA, subjectB).Should().Be(expected);
            (subjectA > subjectB).Should().Be(expected > 0);
            (subjectA >= subjectB).Should().Be(expected >= 0);
            (subjectA < subjectB).Should().Be(expected < 0);
            (subjectA <= subjectB).Should().Be(expected <= 0);

            int invertedExpected = expected * -1;
            subjectB.CompareTo(subjectA).Should().Be(invertedExpected);
            subjectB.CompareTo((object) subjectA).Should().Be(invertedExpected);
            SubStringComparer.Default.Compare(subjectB, subjectA).Should().Be(invertedExpected);
            (subjectB > subjectA).Should().Be(invertedExpected > 0);
            (subjectB >= subjectA).Should().Be(invertedExpected >= 0);
            (subjectB < subjectA).Should().Be(invertedExpected < 0);
            (subjectB <= subjectA).Should().Be(invertedExpected <= 0);
        }
    }
}
