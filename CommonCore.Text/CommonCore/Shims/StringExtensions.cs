﻿#if !NETSTANDARD2_1_OR_GREATER
#region Copyright (c) 2021 Jay Jeckel
// Copyright (c) 2021 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System.Diagnostics.CodeAnalysis;
using CommonCore;

namespace System
{
    /// <summary>
    /// The <see cref="StringExtensions"/> <c>static</c> <c>extension</c> <c>class</c>
    /// provides shim methods to smooth over differences between net standard 2.0 and 2.1.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class StringExtensions
    {
        /// <summary>
        /// Returns a value indicating whether a specified character occurs within this string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The character to seek.</param>
        /// <returns><c>true</c> if the value parameter occurs within this string; otherwise, <c>false</c>.</returns>
        public static bool Contains(this string self, char value)
        {
            Throw.If.SelfNull(self);
            for (int index = 0; index < self.Length; index++)
            { if (self[index] == value) { return true; } }
            return false;
        }
    }
}

#endif
