﻿#if !NETSTANDARD2_1_OR_GREATER

using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;

namespace System.Runtime.CompilerServices
{
    /// <summary>
    /// Shim class to enable C# 9 slicing feature.
    /// Uses CommonCore.Shims for implementation.
    /// </summary>
    [ExcludeFromCodeCoverage]
    [EditorBrowsable(EditorBrowsableState.Never)]
    internal static class RuntimeHelpers
    {
        /// <inheritdoc cref="CommonCore.Shims.RuntimeHelpers.GetSubArray{T}(T[], Range)"/>
        public static T[] GetSubArray<T>(T[] array, Range range)
            => CommonCore.Shims.RuntimeHelpers.GetSubArray(array, range);
    }
}

#endif
